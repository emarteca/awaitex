// simple program with an example of oversynchronization
// the objective here is to try and narrow down the criteria for when the oversynchronization is occurring


// conditions under which we need to keep the synch
//	: the result of the await is being used very soon after, in some computation
// how to track: list of lines result of an await is being used in


function doubleAfter2Seconds(x) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x * 2);
    }, 2000);
  });
}

async function addAsync(x) {
  const a = await doubleAfter2Seconds(10);
  const b = await doubleAfter2Seconds(20);
  const c = await doubleAfter2Seconds(30);
  return x + a + b + c;
}


addAsync(10).then((sum) => {
  console.log(sum);
});